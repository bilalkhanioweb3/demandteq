<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class About extends BaseController
{
    public function About()
    {
        return view('about');
    }
}
