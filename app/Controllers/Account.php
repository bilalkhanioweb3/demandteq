<?php

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Models\UserModel;
use App\Models\PostModel;

class Account extends BaseController
{
    private $data;

    public function __construct()
    {
        $session = session();
        $userData = $session->get('isUserLoggedIn');
        $UserModel = new UserModel();
        $PostModel = new PostModel();
        $this->data = array(
            'user' => $UserModel->where('userId', $userData['userId'])->get()->getRow(),
            'activeUsers' => $UserModel->where('status', 'Active')->countAllResults(),
            'totalUsers' => $UserModel->countAllResults(),
            'totalPosts' => $PostModel->where('userId', $userData['userId'])->countAllResults(),

            'title' => 'Enterpriseteq',
            'user' => $UserModel->where('userId', $userData['userId'])->get()->getRow(),
            'postDetails' => $PostModel->post_page()->paginate(10, 'group1'),
            'pager' => $PostModel->pager,
            'currentPage' => $PostModel->pager->getCurrentPage('group1'), // The current page number
            'totalPages'  => $PostModel->pager->getPageCount('group1'),   // The total page count

        );
    }

    public function dashboard()
    {
        $data = $this->data;
        $data['title'] = 'Dashboard | DemandTeq';
        return view('admin/header', $data)
            . view('admin/dashboard')
            . view('admin/footer');
    }

    public function addPost()
    {
        $data = $this->data;
        $data['title'] = 'Add Post | DemandTeq';
        return view('admin/header', $data)
            . view('admin/addPost')
            . view('admin/footer');
    }

    public function createPost()
    {
        $input = $this->validate([
            'postTitle' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Title is required',
                ],
            ],
            'postType' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Type is required',
                ],
            ],
            'image' => [
                'rules' => 'uploaded[image]|max_size[image,300]|max_dims[image,1920,1080]|is_unique[post.postImg]',
                'errors' => [
                    'uploaded' => 'Image is required',
                    'max_size' => 'Image size must be less than 300 KB',
                    'max_dims' => 'Maximum dimensions size must be 1920 x 1080 px',
                ],
            ],
            'postContent' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Content is required',
                ],
            ],
        ]);

        if (!$input) {
            $data = $this->data;
            $data['title'] = 'Add Post | DemandTeq';
            return view('admin/header', $data, [
                'validation' => $this->validator
            ])
                . view('admin/addPost')
                . view('admin/footer');
        } else {
            $session = session();
            $userData = $session->get('isUserLoggedIn');
            $postTitle = $this->request->getPost('postTitle');

            $slug = trim($postTitle); // trim the string
            $slug = preg_replace('/[^a-zA-Z0-9 -]/', '', $slug); // only take alphanumerical characters, but keep the spaces and dashes too...
            $slug = str_replace(' ', '-', $slug); // replace spaces by dashes
            $slug = strtolower($slug);

            $post_type = trim($this->request->getPost('postType'));
            $post_type = preg_replace('/[^a-zA-Z0-9 -]/', '', $post_type);
            $post_type = str_replace(' ', '-', $post_type);
            $post_type = strtolower($post_type);

            $file = $this->request->getFile('image');
            if ($file->isValid() && !$file->hasMoved()) {
                $newName = $file->getRandomName();
                $path = $this->request->getFile('image')->store('../../public/images/postImages/', $newName);
                $imagePath = base_url() . 'images/postImages/' . $newName;
                $PostModel = new PostModel();
                $array = [
                    'userId' => $userData['userId'],
                    'postTitle' => $this->request->getPost('postTitle'),
                    'postUrl' => $slug,
                    'postType' => $post_type,
                    'postContent' => $this->request->getPost('postContent'),
                    'postImg' => $imagePath,
                    'dateModified' => date("Y-m-d H:i:s"),
                    'dateCreated' => date("Y-m-d H:i:s")
                ];

                // return json_encode($array);
                if ($PostModel->insert($array)) {
                    $session->setTempdata('success', 'Post created successfully', 5);
                    return redirect()->to('account/addPost');
                } else {
                }
            }
        }
    }

    public function managePosts()
    {
        $data = $this->data;
        $data['title'] = 'Dashboard | DemandTeq';
        return view('admin/header', $data)
            . view('admin/managePosts')
            . view('admin/footer');
    }

    public function managePostsModify()
    {
        $session = session();

        $input = $this->validate([
            'postTitle' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Title is required',
                ],
            ],
            'postType' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Type is required',
                ],
            ],
            'image' => [
                'rules' => 'uploaded[image]|max_size[image,300]|max_dims[image,1920,1080]|is_unique[post.postImg]',
                'errors' => [
                    'uploaded' => 'Image is required',
                    'max_size' => 'Image size must be less than 300 KB',
                    'max_dims' => 'Maximum dimensions size must be 1920 x 1080 px',
                ],
            ],
            'postContent' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Content is required',
                ],
            ],
        ]);

        if (!$input) {
            $data = $this->data;
            $data['title'] = 'Add Post | DemandTeq';
            return view('admin/header', $data, [
                'validation' => $this->validator
            ])
                . view('admin/managePosts')
                . view('admin/footer');
        } else {
            $session = session();
            $userData = $session->get('isUserLoggedIn');
            $postTitle = $this->request->getPost('postTitle');

            $slug = trim($postTitle); // trim the string
            $slug = preg_replace('/[^a-zA-Z0-9 -]/', '', $slug); // only take alphanumerical characters, but keep the spaces and dashes too...
            $slug = str_replace(' ', '-', $slug); // replace spaces by dashes
            $slug = strtolower($slug);

            $post_type = trim($this->request->getPost('postType'));
            $post_type = preg_replace('/[^a-zA-Z0-9 -]/', '', $post_type);
            $post_type = str_replace(' ', '-', $post_type);
            $post_type = strtolower($post_type);
            $file = $this->request->getFiles('images');

            if ($file) {

                foreach ($file['images'] as $img) {
                    if ($img->isValid() && !$img->hasMoved()) {

                        $name = $img->getName();
                        $imagePath = base_url() . 'media/' . $name;
                        $img->move(WRITEPATH . '../public/media', $name);

                        $PostModel = new PostModel();

                        $array_data = array(
                            'userId' => $userData['userId'],
                            'postTitle' => $this->request->getPost('postTitle'),
                            'postType' => $this->request->getPost('postType'),
                            'postContent' => $this->request->getPost('postContent'),
                            'postImg' => $imagePath,
                            'dateModified' => date("Y-m-d H:i:s"),
                            'dateCreated' => date("Y-m-d H:i:s")
                        );

                        // print_r($array_data);

                        $PostModel->set($array_data);
                        $PostModel->where('postId', $this->request->getPost('postId'));

                        if ($PostModel->update($array_data)) {
                            $session->setTempdata('success', 'Post Updated successfully', 5);
                            return redirect()->to('account/managePosts');
                        } else {
                            $session->setTempdata('errorMsg', 'Error', 5);

                            return redirect()->to('account/managePosts');
                        }
                    }
                }
            }
        }
    }

    public function deletePost($id)
    {
        $session = session();

        $PostModel = new PostModel();

        $PostModel->where('postId', $id);

        if ($PostModel->delete()) {
            $session->setTempdata('success', 'Post deleted successfully', 5);

            return redirect()->to('account/managePosts');
        } else {
            $session->setTempdata('error', 'Error', 5);

            return redirect()->to('account/managePosts');
        }
    }
}
