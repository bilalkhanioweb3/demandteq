<?php

namespace App\Controllers;

use App\Models\UserModel;
use App\Models\PostModel;

class Home extends BaseController
{
    private $data;

    public function __construct()
    {
        $session = session();
        $userData = $session->get('isUserLoggedIn');
        $UserModel = new UserModel();
        $PostModel = new PostModel();
        $this->data = array(
            'postType' => $PostModel->select('postType')->distinct()->get()->getResult(),
            'todaysPick' => $PostModel->orderBy('postId', 'DESC')->limit(1)->get()->getResult(),
            'todaysPick2' => $PostModel->orderBy('dateCreated', 'DESC')->orderBy('postId', 'RANDOM')->limit(3)->get()->getResult(),
            'latestPosts' => $PostModel->orderBy('dateCreated', 'DESC')->limit(10)->get()->getResult(),
        );
    }

    public function index()
    {
        $data = $this->data;
        $data['twitter_title'] = 'DemandTeq';
        $data['twitter_description'] = '';
        $data['og_title'] = '';
        $data['og_description'] = '';
        $data['og_image'] = base_url('images/logo/logo.png');
        $data['og_url'] = base_url();
        $data['meta_image'] = base_url('images/logo/logo.png');

        $data['meta_title'] = 'DemandTeq';
        $data['meta_description'] = "DemandTeq";
        $data['keywords'] = "";
        $data['title'] = 'DemandTeq';
        $data['canonical'] = '<link rel="canonical" href="' . base_url() . '">';
        $data['schema'] = '';
        return view('header', $data)
            . view('home')
            . view('footer');
    }
}
