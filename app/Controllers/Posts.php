<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\UserModel;
use App\Models\PostModel;

class Posts extends BaseController
{
    private $data;

    public function __construct()
    {
        $session = session();
        $userData = $session->get('isUserLoggedIn');
        $UserModel = new UserModel();
        $PostModel = new PostModel();
        $this->data = array(
            'postType' => $PostModel->select('postType')->distinct()->get()->getResult(),
        );
    }

    public function modifypost($url)
    {

        return view('postModify');
    }

    public function view($url)
    {
        $UserModel = new UserModel();
        $PostModel = new PostModel();
        $getPost = $PostModel->where('postUrl', $url)->get()->getRow();
        if ($getPost) {
            $data = $this->data;
            $data['post'] = $getPost;
            $data['fullName'] = $this->getFullNameByUserId($getPost->userId);
            $data['twitter_title'] = $getPost->postTitle;
            $data['twitter_description'] = $getPost->postTitle;
            $data['og_title'] = $getPost->postTitle;
            $data['og_description'] = $getPost->postTitle;
            $data['og_image'] = $getPost->postImg;
            $data['og_url'] = base_url('news/' . $getPost->postUrl);
            $data['meta_image'] = $getPost->postImg;
            $data['meta_title'] = $getPost->postTitle;
            $data['meta_description'] = $getPost->postTitle;
            $data['keywords'] = $getPost->postTitle;
            $data['title'] = $getPost->postTitle;
            $data['canonical'] = '<link rel="canonical" href="' . base_url('news/' . $getPost->postUrl) . '">';
            $data['schema'] = '';
            return view('header', $data)
                . view('viewPost')
                . view('footer');
        }
    }

    public function category($url)
    {
        $postTypeName = str_replace('-', ' ', $url);
        $postTypeName = ucwords($postTypeName);

        $PostModel = new PostModel();
        $data = $this->data;
        $data['categoryName'] = $postTypeName;
        $data['post'] = $PostModel->where('postType', $url)->get()->getResult();
        $data['twitter_title'] = $postTypeName;
        $data['twitter_description'] = '';
        $data['og_title'] = $postTypeName;
        $data['og_description'] = '';
        $data['og_image'] = base_url('images/logo/logo.png');
        $data['og_url'] = base_url('category/' . $url);
        $data['meta_image'] = base_url('images/logo/logo.png');
        $data['meta_title'] = $postTypeName;
        $data['meta_description'] = '';
        $data['keywords'] = '';
        $data['title'] = $postTypeName;
        $data['canonical'] = '<link rel="canonical" href="' . base_url('category/' . $url) . '">';
        $data['schema'] = '';
        return view('header', $data)
            . view('viewCategoryPost')
            . view('footer');
    }

    public function getFullNameByUserId($userId)
    {
        $UserModel = new UserModel();
        $getUser = $UserModel->where('userId', $userId)->get()->getRow();
        if ($getUser) {
            return $getUser->fullName;
        } else {
            return NULL;
        }
    }
}
