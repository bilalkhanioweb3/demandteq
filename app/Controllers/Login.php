<?php

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Models\UserModel;

class Login extends BaseController
{
    public function index()
    {
        $session = session();

        if (isset($_SESSION['isUserLoggedIn'])) {
            return redirect()->to('account');
        } else {

            $data['title'] = 'Login';
            return view('login', $data);
        }
    }

    public function logIn()
    {
        $input = $this->validate([
            'email' => [
                'rules' => 'required|valid_email',
                'errors' => [
                    'required' => 'Email Address is required',
                    'valid_email' => 'Invalid Email Address',
                ],
            ],
            'password' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Password is required',
                ],
            ],
        ]);

        if (!$input) {
            $data['title'] = 'Sign In | MartechEdge';
            return view('login', $data, [
                'validation' => $this->validator
            ]);
        } else {

            $session = session();

            $UserModel = new UserModel();
            $email = $this->request->getPost('email');
            $password = $this->request->getPost('password');
            $data = $UserModel->where('email', $email)->where('password', $password)->get()->getRow();

            // return json_encode($email);
            // return json_encode($password);

            if ($data) {
                if ($data->status == 'Provisioned') {
                    $session->setTempdata('error', 'Your account is pending verification', 10);
                    return redirect()->to('sign-in');
                } else {
                    $pass = $data->password;
                    // $authenticatePassword = password_verify($password, $pass);
                    // if ($password == $data->password) {
                    $date = date("Y-m-d H:i:s");
                    $UserModel->set('dateModified', $date);
                    $UserModel->where('userId', $data->userId);
                    $UserModel->update();

                    $ses_data = [
                        'fullName' => $data->fullName,
                        'userId' => $data->userId,
                        'email' => $data->email,
                        'isLoggedIn' => TRUE
                    ];
                    echo $session->set('isUserLoggedIn', $ses_data);
                    return redirect()->to('account');
                    // } else {
                    //     $session->setTempdata('error', 'Password is incorrect.', 15);
                    //     return redirect()->to('sign-in');
                    // }
                }
            } else {
                $session->setTempdata('error', 'Email or Password is incorrect.', 10);
                return redirect()->to('sign-in');
            }
        }
    }

    public function logout()
    {
        $session = session();
        $session->remove('isUserLoggedIn');
        return redirect()->to('sign-in');
    }
}
