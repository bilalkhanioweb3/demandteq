<?php

namespace App\Controllers;

use App\Controllers\BaseController;

use App\Models\UserModel;

class Register extends BaseController
{
    public function index()
    {
        $session = session();

        if (isset($_SESSION['isUserLoggedIn'])) {
            return redirect()->to('account');
        } else {

            $data['title'] = 'Create an account | DemandTeq';
            return view('register', $data);
        }
    }

    public function signUp()
    {
        $input = $this->validate([
            'fullName' => [
                'rules' => 'required|alpha_space',
                'errors' => [
                    'required' => 'Full Name is required',
                    'alpha_space' => 'Invalid Email Address',
                ],
            ],
            'email' => [
                'rules' => 'required|valid_email|is_unique[user.email]',
                'errors' => [
                    'required' => 'Email Address is required',
                    'valid_email' => 'Invalid Email Address',
                    'is_unique' => 'Email Address already in use',
                ],
            ],
            'password' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'Password is required',
                ],
            ],
        ]);

        if (!$input) {
            $data['title'] = 'Create an account | DemandTeq';
            return view('register', $data, [
                'validation' => $this->validator
            ]);
        } else {

            $session = session();

            $UserModel = new UserModel();
            $fullName = $this->request->getPost('fullName');
            $email = $this->request->getPost('email');
            $password = $this->request->getPost('password');

            $array = [
                'fullName' => $fullName,
                'email' => $email,
                'password' => $password,
                'roleId' => '0',
                'status' => 'Provisioned',
                'profileImage' => base_url('images/profileImages/default.jpg'),
                'dateModified' => date("Y-m-d H:i:s"),
                'dateCreated' => date("Y-m-d H:i:s")
            ];

            if ($UserModel->insert($array)) {
                // $ses_data = [
                //     'fullName' => $fullName,
                //     'email' => $email,
                //     'isLoggedIn' => TRUE
                // ];
                // echo $session->set('isUserLoggedIn', $ses_data);
                // return redirect()->to('account');

                $session->setTempdata('success', 'Thank you for creating an account. Our team will get back to you shortly.', 10);
                return redirect()->to('register');
            } else {
                $session->setTempdata('error', 'Something went Wrong', 10);
                return redirect()->to('register');
            }

            $data = $UserModel->where('email', $email)->where('password', $password)->get()->getRow();

            // return json_encode($email);
            // return json_encode($password);

            if ($data) {
                $pass = $data->password;
                // $authenticatePassword = password_verify($password, $pass);
                // if ($password == $data->password) {
                $date = date("Y-m-d H:i:s");
                $UserModel->set('dateModified', $date);
                $UserModel->where('userId', $data->userId);
                $UserModel->update();

                $ses_data = [
                    'fullName' => $data->fullName,
                    'email' => $data->email,
                    'isLoggedIn' => TRUE
                ];
                echo $session->set('isUserLoggedIn', $ses_data);
                return redirect()->to('account');
                // } else {
                //     $session->setTempdata('error', 'Password is incorrect.', 15);
                //     return redirect()->to('sign-in');
                // }
            } else {
                $session->setTempdata('error', 'Email or Password is incorrect.', 15);
                return redirect()->to('sign-in');
            }
        }
    }
}
