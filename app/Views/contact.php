<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php  ?></title>
    <link rel="icon" href="<?php echo base_url('images/logo/logo.png'); ?>" type="image/x-icon">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/stylesheet.css'); ?>">
    <style>
        .full-height {
            min-height: 100vh;
        }
    </style>
</head>

<body>
    <?php $validation = \Config\Services::validation(); ?>
    <div class="container p-5">
        <div class="section-title text-center ">
            <h1><span>Get In Touch With Us</h1><br>
            <p> Contact us if you have any questions, or for more information on our services which we are offerings, we will get back to you shortly.</p><br><br>
        </div>
        <div class="row ">
            <div class="col ">
                <label for="name"> Name </label>
                <input type="text" class="form-control" id="name" placeholder="Enter your name">
            </div><br>
            <div class="col">
                <label for="name"> Contact </label>
                <input type="text" class="form-control" id="contact" placeholder="Enter your contact number">
            </div>
        </div><br>
        <div class="row">
            <div class="col">
                <label for="email"> Email </label>
                <input type="email" class="form-control" id="email" placeholder="Enter your email">
            </div><br>
            <div class="col">
                <label for="name"> Subject </label>
                <input type="text" class="form-control" id="subject" placeholder="Enter subject">
            </div>
        </div><br>
        <div class="form-group">
            <label for="message">comment Box</label>
            <textarea class="form-control" id="message" rows="6" placeholder="Write your comment here.."></textarea>
        </div><br>
        <div class="text text-center">
            <button type="submit" class="btn btn-danger">Submit</button>
        </div>
    </div>
    <?php if (session()->getTempdata('success')) : ?>
        <div class="position-relative">
            <div class="position-absolute bottom-0 start-0 ms-2">
                <div class="alert alert-sm bg-success text-white alert-dismissible fade show" role="alert" data-bs-theme="dark">
                    <small><?= session()->getTempdata('success') ?></small>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if (session()->getTempdata('error')) : ?>
        <div class="position-relative">
            <div class="position-absolute bottom-0 start-0 ms-2">
                <div class="alert alert-sm bg-danger text-white alert-dismissible fade show" role="alert" data-bs-theme="dark">
                    <small><?= session()->getTempdata('error') ?></small>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
</body>

</html>