<div class="row">
    <div class="col-lg-12">
        <div class="row mb-5">
            <div class="col-lg-8">
                <?php

                foreach ($todaysPick as $p) {
                    $database = \Config\Database::connect();
                    $builder = $database->table('user');
                    $getUser = $builder->where('userId', $p->userId)->get()->getRow();
                    $fullName = $getUser->fullName;
                ?>
                    <div class="col-lg-12 mb-lg-0 mb-5">
                        <a class="text-decoration-none text-custom" href="<?php echo base_url('news/' . $p->postUrl) ?>">
                            <h5 class="fw-bold mb-3">

                                <?php echo $p->postTitle; ?>

                            </h5>
                        </a>

                        <p><?php echo $fullName; ?></p>
                        <img src="<?php echo $p->postImg; ?>" class="img-fluid">
                    </div>
                <?php } ?>
            </div>
            <div class="col-lg-4">
                <?php

                foreach ($todaysPick2 as $p) {
                    $database = \Config\Database::connect();
                    $builder = $database->table('user');
                    $getUser = $builder->where('userId', $p->userId)->get()->getRow();
                    $fullName = $getUser->fullName;
                ?>
                    <div class="col-lg-12 mb-4 border-bottom">
                        <a class="text-decoration-none text-custom" href="<?php echo base_url('news/' . $p->postUrl) ?>">
                            <h5 class="fw-bold mb-3">
                                <?php echo $p->postTitle; ?>
                            </h5>
                        </a>
                        <p><?php echo $fullName; ?></p>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="row mb-5">
            <div class="col-lg-12 pb-3 mb-3 border-bottom">
                <h6 class="fw-bold">The Latest</h6>
            </div>
            <?php

            foreach ($latestPosts as $p) {
                $database = \Config\Database::connect();
                $builder = $database->table('user');
                $getUser = $builder->where('userId', $p->userId)->get()->getRow();
                $fullName = $getUser->fullName;
            ?>
                <div class="col-lg-4 mb-4 border-bottom">
                    <div class="col mb-5">
                        <div class="col-lg-12">
                            <a class="text-decoration-none text-custom" href="<?php echo base_url('news/' . $p->postUrl) ?>">
                                <div class="col-lg-12 mb-2">
                                    <img src="<?php echo $p->postImg; ?>" class="img-fluid">
                                </div>
                                <h5 class="fw-bold mb-3">
                                    <?php echo $p->postTitle; ?>
                                    </h>
                            </a>
                        </div>
                    </div>
                </div>
            <?php } ?>


        </div>
    </div>
</div>