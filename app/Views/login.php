<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <link rel="icon" href="<?php echo base_url('images/logo/logo.png'); ?>" type="image/x-icon">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/stylesheet.css'); ?>">
    <style>
        .full-height {
            min-height: 100vh;
        }
    </style>
</head>


<body>
    <?php $validation = \Config\Services::validation(); ?>

    <div class=" full-height d-flex align-items-center">
        <div class="container-fluid">


            <div class="row">
                <div class="col-lg-8 my-auto d-lg-block d-none">
                    <div class="row">
                        <div class="col-lg-7 mx-auto text-center">
                            <img src="<?php echo base_url('images/logo/logo.png'); ?>" class="col-5 mb-3">
                            <h1 class="fw-bold">Welcome to DemandTeq</h1>
                            <!-- <h3>Manage your Newsletters</h3>
                            <h3>Access Privacy Settings</h3> -->
                        </div>
                    </div>

                </div>
                <div class="col-lg-4 my-auto py-3">
                    <div class="d-flex justify-content-center">
                        <img src="<?php echo base_url('images/logo/logo.png'); ?>" class="col-3 mb-5">
                    </div>
                    <div class="d-flex justify-content-center">
                        <h5 class="fw-bold mb-5">Sign in</h5>
                    </div>
                    <div class="col-lg-8 mx-auto">
                        <form method="POST" action="<?php echo base_url('log-in'); ?>">
                            <div class="mb-3">
                                <input type="email" name="email" class="form-control border-bottom border-0 rounded-0" placeholder="Email Address">
                            </div>
                            <?php if ($validation->getError('email')) { ?>
                                <p class='text-danger mt-2'>
                                    <?= $error = $validation->getError('email'); ?>
                                </p>
                            <?php } ?>
                            <div class="mb-3">
                                <input type="password" name="password" class="form-control border-bottom border-0 rounded-0" placeholder="Password">
                            </div>
                            <?php if ($validation->getError('password')) { ?>
                                <p class='text-danger mt-2'>
                                    <?= $error = $validation->getError('password'); ?>
                                </p>
                            <?php } ?>
                            <div class="d-grid gap-2 mb-3">
                                <button type="submit" class="btn btn-primary rounded-0">Sign in</button>
                            </div>
                        </form>
                        <div class="text-end mb-3">
                            <a href="#" class="text-decoration-none fw-light text-dark">Forgot Password?</a>
                        </div>
                        <p class="text-center mb-3">Or</p>
                        <div class="d-grid gap-2 mb-3">
                            <a href="<?php echo base_url('register'); ?>" class="btn btn-outline-primary rounded-0">Create an account</a>
                        </div>

                    </div>
                </div>
            </div>



        </div>
    </div>
    <?php if (session()->getTempdata('error')) : ?>
        <div class="position-relative">
            <div class="position-absolute bottom-0 start-0 ms-2">
                <div class="alert alert-sm bg-danger text-white alert-dismissible fade show" role="alert" data-bs-theme="dark">
                    <small><?= session()->getTempdata('error') ?></small>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
</body>

</html>