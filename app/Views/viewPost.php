<div class="row ">
    <div class="col-lg-12 mt-5 mb-3">
        <p></p>
        <h3 class="fw-bold mt-5 mb-5">
            <h2> <?php echo $post->postTitle; ?></h2>
        </h3>
        <p>
            <?php echo $fullName; ?> <?php echo date("j M Y", strtotime($post->dateCreated)); ?>
        </p>
        <div>
            <img src="<?php echo $post->postImg; ?>" class="img-fluid">
        </div>
        <div>
            <?php echo $post->postContent; ?>
        </div>
    </div>
</div>