<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google" content="nositelinkssearchbox">

    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="<?php echo $twitter_title; ?>">
    <meta name="twitter:description" content="<?php echo $twitter_description; ?>">

    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="<?php echo $og_title; ?>">
    <meta name="og:description" content="<?php echo $og_description; ?>">
    <meta name="image" property="og:image" content="<?php echo $og_image; ?>" alt="MarTech Edge">
    <meta name="og:url" content="<?php echo $og_url; ?>">
    <meta name="og:site_name" content="MarTech Edge">
    <meta name="fb:admins" content="">
    <meta name="fb:app_id" content="">
    <meta name="og:type" content="website">

    <!-- Search Meta Tags -->
    <meta name="title" content="<?php echo $meta_title; ?>">
    <meta name="description" content="<?php echo $meta_description; ?>">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <?php echo $canonical; ?>
    <?php echo $schema; ?>
    <!-- Google tag (gtag.js) -->

    <!-- Google Tag Manager -->

    <!-- Google Tag Manager (noscript) -->

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
    <link rel="icon" href="<?php echo base_url('images/logo/logo.png'); ?>" type="image/x-icon">
    <title><?php echo $title; ?></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/stylesheet.css'); ?>">
</head>

<body>
    <nav class="navbar navbar-expand-lg bg-body-tertiary d-lg-none d-block sticky-top">
        <div class="container-fluid">

            <button class="navbar-toggler" data-bs-toggle="offcanvas" href="#offcanvasExample" role="button" aria-controls="offcanvasExample">
                <i class="fa-solid fa-bars"></i>
            </button>
            <a class="navbar-brand" href="#"><img src="<?php echo base_url('images/logo/logo.png'); ?>" width="150"></a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Dropdown
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="#">Action</a></li>
                            <li><a class="dropdown-item" href="#">Another action</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled">Disabled</a>
                    </li>
                </ul>
                <form class="d-flex" role="search">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </div>
    </nav>

    <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
        <div class="offcanvas-header">
            <button type="button" class="btn text-reset ms-auto" data-bs-dismiss="offcanvas" aria-label="Close"><i class="fa-solid fa-arrow-left-long"></i></button>
        </div>
        <div class="offcanvas-body">
            <ul class="nav flex-column sidebar">
                <?php
                if (session()->get('isUserLoggedIn')) {
                ?>
                    <li class="nav-item">
                        <a class="nav-link sliding-border" href="<?php echo base_url('account') ?>">My Account</a>
                    </li>

                <?php
                } else {
                ?>
                    <li class="nav-item">
                        <a class="nav-link sliding-border" href="<?php echo base_url('sign-in') ?>">Login</a>
                    </li>
                <?php } ?>

                <li class="nav-item">
                    <a class="nav-link" href="#">AI</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Startup</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link sliding-border">Newsletters</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link sliding-border">Contact Us</a>
                </li>

            </ul>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2 d-lg-block d-none">
                <div class="sticky-top py-3">
                    <img src="<?php echo base_url('images/logo/logo.png'); ?>" class="img-fluid mb-3 pe-2">
                    <ul class="nav flex-column sidebar">
                        <li class="nav-item">
                            <a class="nav-link sliding-border" href="<?php echo base_url() ?>">Home</a>
                        </li>
                        <?php
                        if (session()->get('isUserLoggedIn')) {
                        ?>

                            <li class="nav-item">
                                <a class="nav-link sliding-border" href="<?php echo base_url('account') ?>">My Account</a>
                            </li>

                        <?php
                        } else {
                        ?>
                            <li class="nav-item">
                                <a class="nav-link sliding-border" href="<?php echo base_url('sign-in') ?>">Login</a>
                            </li>
                        <?php } ?>
                        <?php
                        foreach ($postType as $p) {
                            $postTypeName = str_replace('-', ' ', $p->postType);
                            $postTypeName = ucwords($postTypeName);
                        ?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url('category/' . $p->postType) ?>"><?php echo $postTypeName; ?></a>
                            </li>
                        <?php } ?>
                        <li class="nav-item">
                            <a class="nav-link sliding-border">Newsletters</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-10 col-12">
                <div class="row">
                    <div class="col-lg-4 py-2 ms-auto">
                        <form class="d-flex" role="search">
                            <input class="form-control rounded-0 border-0 border-bottom" type="search" placeholder="Search" aria-label="Search">
                        </form>
                    </div>
                </div>



                <div class="row">
                    <div class="col-lg-2 order-lg-last d-lg-block d-none">
                        <div class="sticky-top py-3">
                            <img src="<?php echo base_url('images/banner/image-2.jpg') ?>" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="row mb-3">
                            <div class="col-lg-12 py-2 ms-auto">
                                <img src="<?php echo base_url('images/banner/image-1.jpg') ?>" class="img-fluid">
                            </div>
                        </div>