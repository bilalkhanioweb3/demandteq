<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php  ?></title>
    <link rel="icon" href="<?php echo base_url('images/logo/logo.png'); ?>" type="image/x-icon">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/stylesheet.css'); ?>">
    <style>
        .full-height {
            min-height: 100vh;
        }
    </style>
</head>

<body>
    <?php $validation = \Config\Services::validation(); ?>
    <h1>hello</h1>

    <?php if (session()->getTempdata('success')) : ?>
        <div class="position-relative">
            <div class="position-absolute bottom-0 start-0 ms-2">
                <div class="alert alert-sm bg-success text-white alert-dismissible fade show" role="alert" data-bs-theme="dark">
                    <small><?= session()->getTempdata('success') ?></small>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if (session()->getTempdata('error')) : ?>
        <div class="position-relative">
            <div class="position-absolute bottom-0 start-0 ms-2">
                <div class="alert alert-sm bg-danger text-white alert-dismissible fade show" role="alert" data-bs-theme="dark">
                    <small><?= session()->getTempdata('error') ?></small>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
</body>

</html>