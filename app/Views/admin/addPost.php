<div class="row">
    <div class="col-lg-12">
        <h2 class="fw-bold mb-3">Add Post</h2>
    </div>
</div>
<?php $validation = \Config\Services::validation(); ?>
<div class="row">
    <div class="col-lg-10">
        <form method="POST" action="<?php echo base_url('account/createPost'); ?>" enctype="multipart/form-data">
            <div class="mb-3">
                <input type="text" class="form-control rounded-0 border-0 border-bottom" name="postTitle" placeholder="Title">
            </div>
            <?php if ($validation->getError('postTitle')) { ?>
                <p class='text-danger mt-2'>
                    <?= $error = $validation->getError('postTitle'); ?>
                </p>
            <?php } ?>
            <div class="mb-3">
                <select class="form-select rounded-0 border-0 border-bottom" name="postType" aria-label="Default select example">
                    <option value="">Select Category</option>
                    <!-- <option value="Advertising">Advertising</option> -->
                    <option value="Artificial Intelligence">Artificial Intelligence</option>
                    <!-- <option value="Machine Learning">Machine Learning</option>
                    <option value="Audio Advertising">Audio Advertising</option>
                    <option value="Business">Business</option>
                    <option value="Technology">Technology</option>
                    <option value="Content Marketing">Content Marketing</option>
                    <option value="Customer Experience Management">Customer Experience Management</option>
                    <option value="Digital Asset Management">Digital Asset Management</option>
                    <option value="Identity Management">Identity Management</option>
                    <option value="Analytics">Analytics</option>
                    <option value="Automation">Automation</option>
                    <option value="Business Intelligence">Business Intelligence</option>
                    <option value="Communications">Communications</option>
                    <option value="Content Syndication">Content Syndication</option>
                    <option value="Customer Identity Management">Customer Identity Management</option>
                    <option value="Ecommerce and Mobile Ecommerce">Ecommerce and Mobile Ecommerce</option>
                    <option value="Intelligent Assistants">Intelligent Assistants</option>
                    <option value="Apps for Business">Apps for Business</option>
                    <option value="B2B Data">B2B Data</option>
                    <option value="Collaboration">Collaboration</option>
                    <option value="Customer Acquisition">Customer Acquisition</option>
                    <option value="Customer Relationship Management">Customer Relationship Management</option>
                    <option value="Email Marketing">Email Marketing</option>
                    <option value="Interactive Content">Interactive Content</option>
                    <option value="Audience Data">Audience Data</option>
                    <option value="Behavioral Marketing">Behavioral Marketing</option>
                    <option value="Customer Data Platforms">Customer Data Platforms</option>
                    <option value="Entertainment">Entertainment</option>
                    <option value="Marketing">Marketing</option>
                    <option value="Augmented Reality">Augmented Reality</option>
                    <option value="Virtual Reality">Virtual Reality</option>
                    <option value="Brand Safety">Brand Safety</option>
                    <option value="Content Management">Content Management</option>
                    <option value="Customer Engagement">Customer Engagement</option>
                    <option value="Digital Transformation">Digital Transformation</option>
                    <option value="Natural Language Processing">Natural Language Processing</option>
                    <option value="Insights">Insights</option>
                    <option value="Reports">Reports</option>
                    <option value="Financial Technology">Financial Technology</option> -->
                    <option value="Cloud Technology">Cloud Technology</option>
                    <!-- <option value="Data Management">Data Management</option> -->
                    <option value="Cybersecurity">Cybersecurity</option>
                    <option value="Cybersecurity">Blockchain</option>
                    <option value="Cybersecurity">Startup</option>

                </select>
            </div>
            <?php if ($validation->getError('postType')) { ?>
                <p class='text-danger mt-2'>
                    <?= $error = $validation->getError('postType'); ?>
                </p>
            <?php } ?>
            <div class="mb-3">
                <input type="file" class="form-control rounded-0" id="image" name="image">
                <div id="image" class="form-text">For better blogs, upload image in png, jpeg format. Keep image size 1920 x 1080 or less.</div>
            </div>
            <?php if ($validation->getError('image')) { ?>
                <p class='text-danger mt-2'>
                    <?= $error = $validation->getError('image'); ?>
                </p>
            <?php } ?>
            <div class="mb-3">
                <textarea class="form-control rounded-0" id="editor" rows="10" name="postContent"></textarea>
            </div>
            <?php if ($validation->getError('postContent')) { ?>
                <p class='text-danger mt-2'>
                    <?= $error = $validation->getError('postContent'); ?>
                </p>
            <?php } ?>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>