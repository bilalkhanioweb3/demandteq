<div class="d-lg-block d-none">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="fw-bold mb-3">Dashboard</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 mb-3">
            <div class="col-lg-12 border bg-success text-white rounded-3 p-3">
                <h3><?php echo $totalPosts; ?></h3>
                <p class="mb-0">Posts</p>
            </div>
        </div>
        <?php if ($user->roleId == '3') { ?>
            <div class="col-lg-3 mb-3">
                <div class="col-lg-12 border bg-danger text-white rounded-3 p-3">
                    <h3><?php echo $activeUsers; ?></h3>
                    <p class="mb-0">Active Users</p>
                </div>
            </div>
            <div class="col-lg-3 mb-3">
                <div class="col-lg-12 border bg-info text-white rounded-3 p-3">
                    <h3><?php echo $totalUsers; ?></h3>
                    <p class="mb-0">Total Users</p>
                </div>
            </div>
            <div class="col-lg-3 mb-3">
                <div class="col-lg-12 border bg-primary text-white rounded-3 p-3">
                    <h3>5</h3>
                    <p class="mb-0">Roles</p>
                </div>
            </div>
        <?php } ?>
    </div>

    <?php if ($user->roleId == '3') { ?>
        <div class="row d-lg-block d-none">
            <div class="col-lg-8">
                <div class="col-lg-12 p-3 border rounded-3">
                    <canvas id="myChart" height="100"></canvas>
                </div>

            </div>
        </div>
    <?php } ?>
</div>

<div class="d-lg-none d-block">
    <h2>Your Recent Posts</h2>
</div>