    <div class="row">
        <div class="col-lg-12">
            <h2 class="fw-bold mb-3">Manage Posts</h2>
        </div>
    </div>

    <table class="table table-sm">
        <thead>
            <tr>
                <th>Sr.</th>
                <th>Post Title</th>
                <th>Post Type</th>
                <!-- <th>Post content</th> -->
                <th>Post Image</th>
                <th>Date Modefied</th>
                <th>Date Created</th>
                <th>View</th>
                <th>Delete</th>
                <th>Modify</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 0;
            foreach ($postDetails as $pd) {
                $i++;
            ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $pd['postTitle']; ?></td>
                    <td>
                        <?php
                        $postTypeName = str_replace('-', ' ', $pd['postType']);
                        $postTypeName = ucwords($postTypeName);

                        echo $postTypeName;
                        ?>
                    </td>
                    <!-- <td><?php echo $pd['postContent']; ?></td> -->
                    <td><img src="<?php echo $pd['postImg']; ?>" width="80"></td>
                    <td> <?php echo date("j M Y", strtotime($pd['dateModified'])); ?></td>
                    <td> <?php echo date("j M Y", strtotime($pd['dateCreated'])); ?></td> <!-- <td>View</td> -->

                    <td>
                        <a href="<?php echo base_url('posts/view/' . $pd['postId']); ?>" class="btn btn-success">View</a>
                    </td>

                    <td>
                        <a href="<?php echo base_url('account/deletePost/' . $pd['postId']); ?>" class="btn btn-danger btn-sm">Delete</a>
                    </td>
                    <td>
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal<?php echo $pd['postId']; ?>">
                            Modify
                        </button>
                    </td>
                </tr>



                </div>
                </div>
                </div>


                <!-- <div class="modal fade" id="exampleModal<?php echo $pd['postId']; ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hiren="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h1 class="modal-title fs-5"> Add / Modify Member</h1>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form action="<?php echo base_url('account/managePostsModify') ?>" method="post">
                                    <div class="mb-3">
                                        <input type="hidden" name="postId" value="<?php echo $pd['postId']; ?>">
                                        <label for="exampleInputEmail1" class="form-label">Post Title</label>
                                        <input type="text" class="form-control mb-3" name="postTitle" value="<?php echo $pd['postTitle']; ?>">
                                        <label for="exampleInputEmail1" class="form-label">Post Type</label>
                                        <input type="text" class="form-control mb-3" name="postType" value="<?php echo $pd['postType']; ?>">
                                        <label for="exampleInputEmail1" class="form-label">Post content</label>
                                        <input type="text" class="form-control mb-3" name="postContent" value="<?php echo $pd['postContent']; ?>">
                                        <label for="exampleInputEmail1" class="form-label">Post Image</label>
                                        <input type="text" class="form-control mb-3" name="postImg" value="<?php echo $pd['postImg']; ?>">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div> -->
            <?php
            }
            ?>
        </tbody>
    </table>

    <!-- Pagination -->
    <div class="pagination justify-content-center mb-4">
        <?php if (!empty($pager)) :
        //echo $pager->simpleLinks('group1', 'bs_simple');
        // echo $pager->links('group1', 'bs_full');
        endif ?>

        <!-- Bootstrap 4.5.2 code to show page 1 of 4 total pages using a button. -->
        <div class="btn-group pagination justify-content-center mb-4" role="group" aria-label="pager counts">
            &nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-light"><?= 'Page ' . $currentPage . ' of ' . $totalPages; ?></button>
        </div>
    </div>

    <script>
        var textarea = document.getElementById('editor');
        var contentInput = document.getElementById('content');

        var editor = sceditor.create(textarea, {
            format: 'bbcode',
            style: '<?= base_url('sceditor/themes/content/default.min.css') ?>'
        });

        editor.on('valuechanged', function() {
            var htmlContent = editor.fromBBCode(editor.val());
            contentInput.value = htmlContent;
        });
    </script>