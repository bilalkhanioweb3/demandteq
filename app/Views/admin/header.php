<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <link rel="icon" href="<?php echo base_url('images/logo/logo.png'); ?>" type="image/x-icon">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/stylesheet.css'); ?>">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: '#editor'
        });
    </script>
</head>


<body>
    <?php $validation = \Config\Services::validation(); ?>
    <nav class="navbar navbar-expand-lg bg-white sticky-top">
        <div class="container-fluid">
            <a class="navbar-brand d-lg-none d-block" data-bs-toggle="offcanvas" href="#offcanvasExample" role="button" aria-controls="offcanvasExample"><img src="<?php echo $user->profileImage; ?>" width="40"></a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <img src="<?php echo $user->profileImage; ?>" width="40" class="rounded-circle me-2"><?php echo $user->fullName; ?><i class="fas fa-caret-down ms-2"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-end p-2">
                            <li>
                                <div class="d-grid gap-2 mb-2">
                                    <a class="btn btn-outline-primary btn-sm" href="#">View Profile</a>
                                </div>
                            </li>
                            <li><a class="dropdown-item" href="#">Settings & Privacy</a></li>
                            <li>
                                <hr class="dropdown-divider">
                            </li>
                            <li><a class="dropdown-item" href="<?php echo base_url('sign-out') ?>">Sign out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
        <div class="offcanvas-header">
            <button type="button" class="btn text-reset ms-auto" data-bs-dismiss="offcanvas" aria-label="Close"><i class="fa-solid fa-arrow-left-long"></i></button>
        </div>
        <div class="offcanvas-body">
            <div class="mb-5">
                <img src="<?php echo $user->profileImage; ?>" width="100" class="mb-3 rounded-circle">
                <h2 class="fw-bold"><?php echo $user->fullName; ?></h2>
                <h5>
                    <a href="#" class="text-decoration-none text-dark">View Profile</a>
                </h5>
            </div>

            <div class="mb-5">
                <h5 class="mb-3">
                    <a href="<?php echo base_url('account'); ?>" class="fw-bold text-decoration-none text-dark mb-3">Dashboard</a>
                </h5>
                <h5 class="mb-3">
                    <a href="<?php echo base_url('account/managePosts'); ?>" class="fw-bold text-decoration-none text-dark mb-3">Posts</a>
                </h5>

                <?php if ($user->roleId == '3') { ?>
                    <h5 class="mb-3">
                        <a href="#" class="fw-bold text-decoration-none text-dark mb-3">User</a>
                    </h5>
                    <h5 class="mb-3">
                        <a href="#" class="fw-bold text-decoration-none text-dark mb-3">Roles</a>
                    </h5>
                <?php } ?>
                <h5 class="mb-3">
                    <a href="#" class="fw-bold text-decoration-none text-dark mb-3">Settings & Privacy</a>
                </h5>
                <h5 class="mb-3">
                    <a href="<?php echo base_url('sign-out') ?>" class="fw-bold text-decoration-none text-dark mb-3">Sign Out</a>
                </h5>
            </div>

            <!-- <ul class="nav flex-column sidebar">
                <li class="nav-item">
                    <a class="nav-link sliding-border" href="<?php echo base_url('admin-account'); ?>">Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link sliding-border" data-bs-toggle="collapse" href="#posts" aria-expanded="false" aria-controls="posts">Posts</a>
                    <div class="collapse" id="posts">
                        <ul class="nav flex-column ms-4 py-2">
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url('admin-account/manage-post'); ?>">Add Post</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url('admin-account/manage-post'); ?>">Manage Posts</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <?php if ($user->roleId == '3') { ?>
                    <li class="nav-item">
                        <a class="nav-link sliding-border" data-bs-toggle="collapse" href="#author" aria-expanded="false" aria-controls="author">Author</a>
                        <div class="collapse" id="author">
                            <ul class="nav flex-column ms-4 py-2">
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url('admin-account/add-author'); ?>">Add Author</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url('admin-account/manage-author'); ?>">Manage Author</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link sliding-border" data-bs-toggle="collapse" href="#user" aria-expanded="false" aria-controls="user">User</a>
                        <div class="collapse" id="user">
                            <ul class="nav flex-column ms-4 py-2">
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url('admin-account/manage-user'); ?>">Manage User</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link sliding-border" href="<?php echo base_url('account/roles'); ?>">Roles</a>
                    </li>
                <?php } ?>



            </ul> -->
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2 px-4 py-3 d-lg-block d-none">
                <ul class="nav flex-column sidebar">
                    <li class="nav-item">
                        <a class="nav-link sliding-border" href="<?php echo base_url('account'); ?>">Dashboard</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link sliding-border" data-bs-toggle="collapse" href="#posts" aria-expanded="false" aria-controls="posts">Posts</a>
                        <div class="collapse" id="posts">
                            <ul class="nav flex-column ms-4 py-2">
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url('account/addPost'); ?>">Add Post</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo base_url('account/managePosts'); ?>">Manage Posts</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <?php if ($user->roleId == '3') { ?>
                        <li class="nav-item">
                            <a class="nav-link sliding-border" data-bs-toggle="collapse" href="#user" aria-expanded="false" aria-controls="user">User</a>
                            <div class="collapse" id="user">
                                <ul class="nav flex-column ms-4 py-2">
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo base_url('admin-account/manage-user'); ?>">Manage User</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link sliding-border" href="<?php echo base_url('account/roles'); ?>">Roles</a>
                        </li>
                    <?php } ?>

                </ul>
            </div>

            <div class="col-lg-10 p-4" style="min-height:90vh;">