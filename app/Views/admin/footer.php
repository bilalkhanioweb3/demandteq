</div>
</div>
</div>
<?php if (session()->getTempdata('success')) : ?>
    <div class="position-relative">
        <div class="position-absolute bottom-0 start-0 ms-2">
            <div class="alert alert-sm bg-success text-white alert-dismissible fade show" role="alert" data-bs-theme="dark">
                <small><?= session()->getTempdata('success') ?></small>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (session()->getTempdata('error')) : ?>
    <div class="position-relative">
        <div class="position-absolute bottom-0 start-0 ms-2">
            <div class="alert alert-sm bg-danger text-white alert-dismissible fade show" role="alert" data-bs-theme="dark">
                <small><?= session()->getTempdata('error') ?></small>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="fixed-bottom bg-white d-lg-none d-block border-top">
    <div class="container-fluid py-3">
        <div class="row">
            <div class="col-lg-12">
                <ul class="nav nav-pills nav-fill">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('account') ?>"><i class="fa-solid fa-display fa-2x"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('account/addPost') ?>"><i class="fa-regular fa-square-plus fa-2x"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>
    const ctx = document.getElementById('myChart');

    new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
</body>

</html>