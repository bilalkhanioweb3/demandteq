</div>
</div>
</div>
</div>
</div>
<div class="container-fluid bg-dark text-white py-3">
    <div class="row">
        <div class="col-lg-5 p-3">
            <img src="<?php echo base_url('images/logo/logo-white.png') ?>">
            <div class="p-2">
                <p class="mb-3">
                    DemandTeq is where tomorrow is realized. It is the essential
                    source of information and ideas that make sense of a world
                    in constant transformation. The DemandTeq conversation illuminates
                    how technology is changing every aspect of our lives—from culture to
                    business, science to design. The breakthroughs and innovations that
                    we uncover lead to new ways of thinking, new connections, and new industries.
                </p>
                <div class="mt-3">
                    <a href="#" class="text-white"><i class="fa-brands  fa-facebook-f me-3"></i></a>
                    <a href="#" class="text-white"><i class="fa-brands  fa-twitter me-3"></i></a>
                    <a href="#" class="text-white"><i class="fa-brands fa-linkedin-in me-3"></i></a>
                    <a href="#" class="text-white"><i class="fa-brands fa-instagram  me-3"></i></a>

                </div>
            </div>
        </div>

        <div class="col-lg-3 text-decoration-none text-white my-auto ms-auto">
            <h6>Contact</h6>
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a class="nav-link text-decoration-none my-auto ms-auto" href="#">Advertise</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-decoration-none" href="#">Contact Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-decoration-none" href="#">Privacy Policy</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-decoration-none">GDPR</a>
                </li>
            </ul>
        </div>


    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
</body>

</html>